// 表单校验规则由 schema2code 生成，不建议直接修改校验规则，而建议通过 schema2code 生成, 详情: https://uniapp.dcloud.net.cn/uniCloud/schema


const validator = {
  "address": {
    "rules": [
      {
        "format": "string"
      }
    ],
    "label": "地址"
  },
  "addressName": {
    "rules": [
      {
        "format": "string"
      }
    ],
    "label": "地址简称"
  },
  "banner": {
    "rules": [
      {
        "format": "string"
      }
    ],
    "label": "banner"
  },
  "deliveryTypes": {
    "rules": [
      {
        "format": "array"
      },
      {
        "range": [
          {
            "text": "配送到家",
            "value": "deliveryHome"
          },
          {
            "text": "到店/自提点自提",
            "value": "selfRaising"
          }
        ]
      }
    ],
    "defaultValue": "deliveryHome",
    "label": "配送方式"
  },
  "id": {
    "rules": [
      {
        "format": "int"
      }
    ],
    "label": "店铺编号"
  },
  "name": {
    "rules": [
      {
        "format": "string"
      }
    ],
    "label": "店铺名称"
  },
  "notice": {
    "rules": [
      {
        "format": "string"
      }
    ],
    "label": "店铺公告"
  },
  "phone": {
    "rules": [
      {
        "format": "string"
      }
    ],
    "label": "联系电话"
  },
  "serviceTime": {
    "rules": [
      {
        "format": "string"
      }
    ],
    "label": "服务时间"
  },
  "src": {
    "rules": [
      {
        "format": "string"
      }
    ],
    "label": "店铺logo"
  },
  "stationId": {
    "rules": [
      {
        "format": "int"
      }
    ],
    "label": "分站id"
  },
  "business_license": {
    "rules": [
      {
        "format": "string"
      }
    ],
    "label": "营业执照"
  },
  "food_license": {
    "rules": [
      {
        "format": "string"
      }
    ],
    "label": "食品经营许可"
  },
  "state": {
    "rules": [
      {
        "format": "bool"
      }
    ],
    "label": "是否启用"
  },
  "uid": {
    "rules": [
      {
        "format": "string"
      }
    ],
    "defaultValue": {
      "$env": "uid"
    },
    "label": "创建者"
  }
}

const enumConverter = {
  "deliveryTypes_valuetotext": [
    {
      "text": "配送到家",
      "value": "deliveryHome"
    },
    {
      "text": "到店/自提点自提",
      "value": "selfRaising"
    }
  ]
}

function filterToWhere(filter, command) {
  let where = {}
  for (let field in filter) {
    let { type, value } = filter[field]
    switch (type) {
      case "search":
        if (typeof value === 'string' && value.length) {
          where[field] = new RegExp(value)
        }
        break;
      case "select":
        if (value.length) {
          let selectValue = []
          for (let s of value) {
            selectValue.push(command.eq(s))
          }
          where[field] = command.or(selectValue)
        }
        break;
      case "range":
        if (value.length) {
          let gt = value[0]
          let lt = value[1]
          where[field] = command.and([command.gte(gt), command.lte(lt)])
        }
        break;
      case "date":
        if (value.length) {
          let [s, e] = value
          let startDate = new Date(s)
          let endDate = new Date(e)
          where[field] = command.and([command.gte(startDate), command.lte(endDate)])
        }
        break;
      case "timestamp":
        if (value.length) {
          let [startDate, endDate] = value
          where[field] = command.and([command.gte(startDate), command.lte(endDate)])
        }
        break;
    }
  }
  return where
}

export { validator, enumConverter, filterToWhere }
