export function initPermission(Vue) {
	Vue.prototype.$hasPermission = function hasPermission(name) {
		const permission = this.$store.state.user.userInfo.permission || [];
		const role = this.$store.state.user.userInfo.role || []
		//超级管理员拥有所有权限
		return permission.indexOf(name) > -1 || role.indexOf("admin") > -1;
	}
	Vue.prototype.$hasRole = function hasRole(name) {
		const role = this.$store.state.user.userInfo.role || []
		return role.indexOf(name) > -1
	}
}
