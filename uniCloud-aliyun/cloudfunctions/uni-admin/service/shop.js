const {
	Service
} = require('uni-cloud-router')

module.exports = class ShopService extends Service {

	constructor(ctx) {
		super(ctx)
		this.collection = this.db.collection('cloud_shops')
	}
	async getAdminShops(userInfo) {
		if (!userInfo) {
			return [];
		}
		let conditions = false;
		const cmd = this.db.command;
		if (userInfo.role && userInfo.role.indexOf("admin") != -1) {
			conditions = {
				_id: cmd.exists(true)
			}
		}
		//管理所有店铺
		if (userInfo.admin) {
			conditions = {
				id: cmd.in(userInfo.admin)
			};
		}
		if (!conditions) {
			return [];
		}
		let res = await this.collection.where(conditions).field({
			id: 1,
			name: 1
		}).orderBy("id", "desc").limit(50).get();
		return res.data;
	}
}
