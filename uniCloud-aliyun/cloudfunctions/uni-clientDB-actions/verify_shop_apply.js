// 开发文档：https://uniapp.dcloud.io/uniCloud/clientdb?id=action
const db = uniCloud.database()
const cmd = db.command;
module.exports = {
	before: async (state, event) => {

	},
	after: async (state, event, error, result) => {
		if (error) {
			throw error
		}
		//用户增加管理权限,必须重新登录
		await db.collection("uni-id-users").doc(state.newData.uid).update({
			admin: cmd.push(state.newData.id),
			role: cmd.push("admin"),
			token: [] //强制重新登录
		});
		//调用其他接口

		return result
	}
}
