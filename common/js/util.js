let _debounceTimeout = null,
	_throttleRunning = false;

import config from '@/config'

/**
 * 防抖
 * @param {Function} 执行函数
 * @param {Number} delay 延时ms
 */
export const debounce = (fn, delay = 500) => {
	clearTimeout(_debounceTimeout);
	_debounceTimeout = setTimeout(() => {
		fn();
	}, delay);
}
/**
 * 节流
 * @param {Function} 执行函数
 * @param {Number} delay 延时ms
 */
export const throttle = (fn, delay = 500) => {
	if (_throttleRunning) {
		return;
	}
	_throttleRunning = true;
	fn();
	setTimeout(() => {
		_throttleRunning = false;
	}, delay);
}
/**
 * toast
 */
export const msg = (title = '', param = {}) => {
	if (!title) return;
	uni.showToast({
		title,
		duration: param.duration || 1500,
		mask: param.mask || false,
		icon: param.icon || 'none'
	});
}
export const success = (title = '', param = {}) => {
	if (!title) return;
	uni.showToast({
		title,
		duration: param.duration || 1500,
		mask: param.mask || false,
		icon: 'success'
	});
}
/**
 * 检查登录
 * @return {Boolean}
 */
export const isLogin = (options = {}) => {
	const token = uni.getStorageSync('uniIdToken');
	if (token) {
		return true;
	}
	if (options.nav !== false) {
		uni.navigateTo({
			url: '/pages/auth/login'
		})
	}
	return false;
}
/**
 * 获取页面栈
 * @param {Number} preIndex为1时获取上一页
 * @return {Object}
 */
export const prePage = (preIndex = 1) => {
	const pages = getCurrentPages();
	const prePage = pages[pages.length - (preIndex + 1)];

	return prePage.$vm;
}
/**
 * 格式化时间戳 Y-m-d H:i:s
 * @param {String} format Y-m-d H:i:s
 * @param {Number} timestamp 时间戳
 * @return {String}
 */
export const date = (format, timeStamp) => {
	let _date;
	if (!timeStamp) {
		_date = new Date();
	} else {
		if ('' + timeStamp.length <= 10) {
			timeStamp = +timeStamp * 1000;
		} else {
			timeStamp = +timeStamp;
		}
		_date = new Date(timeStamp);
	}
	let Y = _date.getFullYear(),
		m = _date.getMonth() + 1,
		d = _date.getDate(),
		H = _date.getHours(),
		i = _date.getMinutes(),
		s = _date.getSeconds();

	m = m < 10 ? '0' + m : m;
	d = d < 10 ? '0' + d : d;
	H = H < 10 ? '0' + H : H;
	i = i < 10 ? '0' + i : i;
	s = s < 10 ? '0' + s : s;

	return format.replace(/[YmdHis]/g, key => {
		return {
			Y,
			m,
			d,
			H,
			i,
			s
		} [key];
	});
}
//二维数组去重
export const getUnique = array => {
	let obj = {}
	return array.filter((item, index) => {
		let newItem = item + JSON.stringify(item)
		return obj.hasOwnProperty(newItem) ? false : obj[newItem] = true
	})
}
// 判断类型集合
export const checkStr = (str, type) => {
	switch (type) {
		case 'mobile': //手机号码
			return /^1[3|4|5|6|7|8|9][0-9]{9}$/.test(str);
		case 'tel': //座机
			return /^(0\d{2,3}-\d{7,8})(-\d{1,4})?$/.test(str);
		case 'card': //身份证
			return /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/.test(str);
		case 'mobileCode': //6位数字验证码
			return /^[0-9]{6}$/.test(str)
		case 'pwd': //密码以字母开头，长度在6~18之间，只能包含字母、数字和下划线
			return /^([a-zA-Z0-9_]){6,18}$/.test(str)
		case 'payPwd': //支付密码 6位纯数字
			return /^[0-9]{6}$/.test(str)
		case 'postal': //邮政编码
			return /[1-9]\d{5}(?!\d)/.test(str);
		case 'QQ': //QQ号
			return /^[1-9][0-9]{4,9}$/.test(str);
		case 'email': //邮箱
			return /^[\w-]+(\.[\w-]+)*@[\w-]+(\.[\w-]+)+$/.test(str);
		case 'money': //金额(小数点2位)
			return /^\d*(?:\.\d{0,2})?$/.test(str);
		case 'URL': //网址
			return /(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&:/~\+#]*[\w\-\@?^=%&/~\+#])?/.test(str)
		case 'IP': //IP
			return /((?:(?:25[0-5]|2[0-4]\\d|[01]?\\d?\\d)\\.){3}(?:25[0-5]|2[0-4]\\d|[01]?\\d?\\d))/.test(str);
		case 'date': //日期时间
			return /^(\d{4})\-(\d{2})\-(\d{2}) (\d{2})(?:\:\d{2}|:(\d{2}):(\d{2}))$/.test(str) || /^(\d{4})\-(\d{2})\-(\d{2})$/
				.test(str)
		case 'number': //数字
			return /^[0-9]$/.test(str);
		case 'english': //英文
			return /^[a-zA-Z]+$/.test(str);
		case 'chinese': //中文
			return /^[\\u4E00-\\u9FA5]+$/.test(str);
		case 'lower': //小写
			return /^[a-z]+$/.test(str);
		case 'upper': //大写
			return /^[A-Z]+$/.test(str);
		case 'HTML': //HTML标记
			return /<("[^"]*"|'[^']*'|[^'">])*>/.test(str);
		default:
			return true;
	}
}


/**
 * 批量上传多个文件
 * @param {Object} name 地址前缀
 * @param {Object} number 可选文件个数
 * @param {Object} chooseCallback 选择后回调
 * @param {Object} successCallback 上传完后回调
 */
export const uploadFiles = (name, number, chooseCallback, successCallback) => {
	let dt = new Date();
	let pathArr = [name, date("Y-m-d")];
	uni.chooseImage({
		count: number,
		success: res => {
			let paths = [];
			console.log(res)
			if (chooseCallback && successCallback) {
				chooseCallback(res.tempFilePaths);
			}
			let promises = [];
			uni.showLoading({
				title: "上传中"
			})
			//循环上传
			res.tempFilePaths.map((filePath) => {
				let fname = (Math.random() + '').substr(2) + '.jpg';
				let cpath = pathArr.join('/') + '/' + fname;
				let uploadPromise = cloudUploadFile(filePath, cpath);
				promises.push(uploadPromise)
			})
			Promise.all(promises).then(res => {
				console.log(res);
				uni.hideLoading();
				if (successCallback) {
					successCallback(res);
				} else if (chooseCallback) {
					chooseCallback(res);
				}
			});
		}
	});
}
/**
 * 上传到云端储存
 * @param {File}   filePath 本地文件
 * @param {String}   cpath 云端文件名称，腾讯云为文件实际地址
 * @link https://uniapp.dcloud.io/uniCloud/storage?id=uploadfile
 */
export const cloudUploadFile = (filePath, cpath) => {
	return new Promise((resolve, reject) => {
		let result = uniCloud.uploadFile({
			filePath: filePath,
			cloudPath: cpath,
			onUploadProgress: pro => {},
			success: res => {
				console.log("cloudUploadFile", res)
				if (res.fileID.indexOf("cloud://") != -1) {
					//用这个转换一下，有一个隐藏的好处，可以等待文件发布到cdn，避免立即访问不到
					uniCloud.getTempFileURL({
						fileList: [res.fileID]
					}).then(res2 => {
						resolve(res2.fileList[0].tempFileURL);
					})
				} else {
					//延时返回，可以等待文件发布到cdn，避免立即访问不到
					setTimeout(() => {
						resolve(res.fileID);
					}, 200);
				}
			},
			fail: () => {
				reject(false);
			}
		});
	});
};


/**
 * 计算2点的距离，批量距离计算（矩阵）
 * @param {Object} fromLatlng
 * @param {Object} toLatlng
 * @link https://lbs.qq.com/service/webService/webServiceGuide/webServiceMatrix
 */
export const mapDistance = function(fromLatlng, toLatlng) {
	return new Promise((resolve, reject) => {
		//console.log(fromLatlng)
		uni.request({
			url: `https://apis.map.qq.com/ws/distance/v1/matrix`,
			data: {
				mode: "bicycling",
				from: fromLatlng,
				to: toLatlng,
				key: config.qqmapWxKey
			},
			success: (res) => {
				console.log(res)
				resolve(res.data.result.rows);
			},
			fail: (err) => {
				console.log(err)
				reject();
			}
		})
	})
}
/**
 * 逆地址解析（坐标位置描述）
 * @param {Object} fromLatlng
 * @param {Object} poi_options
 * @link https://lbs.qq.com/service/webService/webServiceGuide/webServiceGcoder
 */
export const getLocationAddress = function(fromLatlng, poi_options) {
	poi_options = poi_options || "address_format=short;policy=5";
	return new Promise((resolve, reject) => {
		//console.log(fromLatlng)
		uni.request({
			url: `https://apis.map.qq.com/ws/geocoder/v1/`,
			withCredentials: true,
			data: {
				location: fromLatlng,
				get_poi: 1,
				poi_options: poi_options,
				key: config.qqmapWxKey
			},
			success: (res) => {
				resolve(res.data.result);
			},
			fail: (err) => {
				console.log(err)
				reject();
			}
		})
	})
}
export const getAddressGeocoder = function(address) {
	return new Promise((resolve, reject) => {
		uni.request({
			url: `https://apis.map.qq.com/ws/geocoder/v1/`,
			data: {
				address: address,
				key: config.qqmapWxKey
			},
			success: (res) => {
				console.log(res.data.result)
				resolve(res.data.result);
			},
			fail: (err) => {
				console.log(err)
				reject();
			}
		})
	})
}

/**
 * 小程序自动登录，只返回授权code
 */
export const micLogin = async () => {
	console.log("小程序用户自动登录开始")
	return new Promise((resolve, reject) => {
		uni.getProvider({
			service: 'oauth',
			success: (res) => {
				console.log(res)
				let provider = res.provider[0];
				uni.login({
					provider: provider,
					success: (loginRes) => {
						console.log('loginRes', loginRes);
						resolve(loginRes.code);
					}
				});
			}
		});
	});
}
